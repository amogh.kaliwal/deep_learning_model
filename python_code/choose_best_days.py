""" choose best days for training """

def best_days_training(train_data, test_data):
    
    train_data['DATA_DATETIME'] = pd.to_datetime(train_data['DATA_DATETIME'])
    train_data['DATES'] = pd.to_datetime(train_data['DATA_DATETIME']).dt.date
    train_data['TIME'] = pd.to_datetime(train_data['DATA_DATETIME']).dt.time
    
    train_data['POWER'].fillna((train_data['POWER'].mean()), inplace = True)
    
    train_data["RADIATION"]=train_data["RADIATION"].apply(lambda x: x / 1361)
    train_data["TEMPERATURE"]=train_data["TEMPERATURE"].apply(lambda x: x / 51)
    train_data["CLOUD_COVER"]=train_data["CLOUD_COVER"].apply(lambda x: x / 100)
    train_data["HUMIDITY"]=train_data["HUMIDITY"].apply(lambda x: x / 100)
    train_data["SURFACE_PRESSURE"]=train_data["SURFACE_PRESSURE"].apply(lambda x: x / 1093)
    
    # subset data for required columns
    train_data = train_data[['BLOCK_ID',
                             'RADIATION',
                             'TEMPERATURE',
                             'CLOUD_COVER',
                             'HUMIDITY',
                             'PRECIP_PROBABILITY',
                             'SURFACE_PRESSURE',
                             'WIND_SPEED',
                             'POWER',
                             'DATES']]
        
    train_datadf = train_data.groupby('DATES').sum()
    
    train_datadf.drop(['BLOCK_ID','POWER'], axis = 1, inplace = True)
    
    test_data['DATA_DATETIME'] = pd.to_datetime(test_data['DATA_DATETIME'])
    
    test_data['DATES'] = pd.to_datetime(test_data['DATA_DATETIME']).dt.date
    test_data['TIME'] = pd.to_datetime(test_data['DATA_DATETIME']).dt.time
    
    test_data["RADIATION"]=test_data["RADIATION"].apply(lambda x: x / 1361)
    test_data["TEMPERATURE"]=test_data["TEMPERATURE"].apply(lambda x: x / 51)
    test_data["CLOUD_COVER"]=test_data["CLOUD_COVER"].apply(lambda x: x / 100)
    test_data["HUMIDITY"]=test_data["HUMIDITY"].apply(lambda x: x / 100)
    test_data["SURFACE_PRESSURE"]=test_data["SURFACE_PRESSURE"].apply(lambda x: x / 1093)
    
    # subset data for required columns
    test_data = test_data[['BLOCK_ID',
                             'RADIATION',
                             'TEMPERATURE',
                             'CLOUD_COVER',
                             'HUMIDITY',
                             'PRECIP_PROBABILITY',
                             'SURFACE_PRESSURE',
                             'WIND_SPEED',
                             'DATES']]
    
    test_datadf = test_data.groupby('DATES').sum()
    
    test_datadf.drop(['BLOCK_ID'], axis = 1, inplace = True)
    
    # this according to elbow method to choose the no of clusters
    # %pip install kneed
    from sklearn.cluster import KMeans
    from kneed import KneeLocator
    from sklearn.metrics import silhouette_score
    from sklearn.preprocessing import StandardScaler
    Kmeans_Kwargs = {"init": "random", "n_init":10,"max_iter": 300, "random_state":42,}
    
    # A list to hold sse values
    sse = []
    for k in range (1,11):
      Kmeans = KMeans(n_clusters=k, **Kmeans_Kwargs)
      Kmeans.fit(train_datadf)
      sse.append(Kmeans.inertia_)
      
    kl = KneeLocator(range(1,11),sse,curve="convex",direction="decreasing")
    opt_clusters = kl.elbow
    
    kmeans = KMeans(
            init="random",
            n_clusters= opt_clusters,
            n_init=10,
            max_iter=300,
            random_state=42)
    
    kmeans.fit(train_datadf)
    
    kmeans.inertia_
    
    train_centroids = kmeans.cluster_centers_
    
    kmeans.n_iter_
    
    # there are only 60 labels here
    kmeans.labels_
    
    train_datadf['CLUSTER'] = kmeans.labels_
    
    from numpy import array
    import numpy as np
    
    test = np.array(test_datadf)
    train = np.transpose(train_centroids)
    train =pd.DataFrame(data=train)
    test = np.transpose(test)
    test  = pd.DataFrame(test)
    
    def calculate_absolute(i,y):
      ds = 0
      for e in i:
        sub = i[e] - y
        if sub.sum() < ds:
          ds = sub.sum()
          output = e
        return e 
    
    value = calculate_absolute(train,test[0])
    
    def find_clusters(value, df, colname):
      exactmatch = df[df[colname]==value]
      return exactmatch
    train_bestdays = find_clusters(value, train_datadf, "CLUSTER")
    
    train_bestdays = train_bestdays.reset_index()
    
    train_bestdays = train_bestdays["DATES"]
    train_bestdays = pd.DataFrame(train_bestdays)
    
    train_merge = pd.merge(train_bestdays, train_data, how='left', on = 'DATES')
    
    
    
    
    
    
    
    
    
    