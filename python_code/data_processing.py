### Terra Form
#nodalId = -1 
#orgId = 135
#pssId = 288
#FxDate = "2021-05-20"
#StartDate = "2021-04-01"
## Regode
#nodalId = 195
#orgId = 27
#pssId = 48
## FxDate = "2021-5-17"
#StartDate = "2021-05-03"
## Sadashivpet
nodalId = 233 
orgId = 32
pssId = 92
FxDate = "2021-05-20"
StartDate = "2020-05-01"

import constants
import data_extraction
import data_loading
import pandas as pd
from datetime import datetime, timedelta, time
import warnings
warnings.filterwarnings("ignore")


# df = df.groupby('Name', as_index=False).agg(Sum1 = ('Missed','sum'), 
#                                             Sum2 = ('Credit','sum'),
#                                             Average = ('Grade','mean'))

# divide the data for training and testing
train_data, test_data = data_loading.divide_train_test(nodalId, 
                                                       orgId, 
                                                       pssId, 
                                                       StartDate, 
                                                       FxDate, 
                                                       constants.headers)

""" subset days where we dont have power value """

def choose_data(data):
    
    # data = train_data
    
    # remove duplicates from data
    data = data.drop_duplicates().reset_index(drop = True)
    
    # create date attribute
    data['DATA_DATE'] = data['DATA_DATETIME'].str.split(" ", 
              n = 1, expand = True)
    
    # identify rows where power is less than 96
    dates_data = data.groupby('DATA_DATE').count()['POWER']
    dates_data = dates_data.reset_index()
    
    # eleminate rows where power is less than 96
    dates_data = dates_data[
            ~(dates_data['POWER'] < (24 * 60
              ) / int(constants.freq_granularity))]
    
    # prepare a data frame with all values
    merged_data = pd.merge(dates_data, data, 
                           on = ['DATA_DATE'], 
                           how = 'left')
    
    # drop unwanted columns
    merged_data.drop(['DATA_DATE', 'POWER_x'], 
                     axis = 'columns', inplace = True)
    
    # rename required columns
    merged_data = merged_data.rename(columns = {'POWER_y': 'POWER'}, 
                                     inplace = False)
    
    # return processed data
    return merged_data
    
""" choose best days for training """

def best_days_training(train_data, test_data):
    
    
    

""" pre-process data for model input """

def data_pre_processing(train_data, test_data):
    
    

################################################################
