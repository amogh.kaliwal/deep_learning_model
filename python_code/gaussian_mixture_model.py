import pandas as pd

# training gaussian mixture model 
from sklearn.mixture import GaussianMixture
gmm = GaussianMixture(n_components = 5)

cluster_data = train_data[['RADIATION', 'TEMPERATURE', 
                          'CLOUD_COVER', 'HUMIDITY', 
                          'PRECIP_PROBABILITY', 
                          'SURFACE_PRESSURE', 'WIND_SPEED']]
gmm.fit(cluster_data)

# predictions from gmm
labels = gmm.predict(cluster_data)
frame = pd.DataFrame(cluster_data)
frame['cluster'] = labels
frame.columns = ['Weight', 'Height', 'cluster']

color=['blue','green','cyan', 'black','red']
for k in range(0,4):
    data = frame[frame["cluster"]==k]
    plt.scatter(data["Weight"],data["Height"],c=color[k])
plt.show()