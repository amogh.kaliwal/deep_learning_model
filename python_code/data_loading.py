import constants
import data_extraction
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

"""      combine data for model building       """

def combine_data(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    """      meteotest radiation       """
#    # extract meteotest radiation data for perticular day
#    meteo_rad = data_extraction.meteotest_weather(nodalId, 
#                                                  orgId, 
#                                                  pssId, 
#                                                  StartDate,
#                                                  FxDate,
#                                                  headers)
    meteo_rad = pd.read_csv(
        constants.base_path + "input_data/extracted_data/" + "Sadashivpet_Meteo_Rad_un-updated.csv")

    """      darksky data       """    
#    # extract darksky data for n-days
#    darksky_data = data_extraction.darksky_weather(nodalId, 
#                                                   orgId, 
#                                                   pssId, 
#                                                   StartDate,
#                                                   FxDate, 
#                                                   headers)
    darksky_data = pd.read_csv(
        constants.base_path + "input_data/extracted_data/" + "Sadashivpet_Darksky_Data.csv")

    """      actual power       """
#    # extract actual power for n-days
#    actual_power = data_extraction.actual_power(nodalId, 
#                                                orgId, 
#                                                pssId, 
#                                                StartDate,
#                                                FxDate,
#                                                headers)
    actual_power = pd.read_csv(
        constants.base_path + "input_data/extracted_data/" + "Sadashivpet_Actual_Power.csv")

    # subset data for required columns
    subset_darksky_data = darksky_data[['BLOCK_ID', 
                                       'CLOUD_COVER', 
                                       'DATA_DATETIME',                                      
                                       'HUMIDITY',
                                       'PRECIP_PROBABILITY',
                                       'SURFACE_PRESSURE',
                                       'WIND_SPEED']]
    
    subset_meteo_rad = meteo_rad[['DATA_DATETIME',
                                  'BLOCK_ID',
                                  'RADIATION',
                                  'TEMPERATURE']]
    
    # combine meteo and dark sky data
    merge_meteo_darksky = pd.merge(subset_meteo_rad, subset_darksky_data, 
                                   on = ['DATA_DATETIME', 'BLOCK_ID'], 
                                   how = 'left')
    
    # combine meteo and dark sky data
    merge_meteo_darksky = pd.merge(merge_meteo_darksky, actual_power, 
                                   on = ['DATA_DATETIME', 'BLOCK_ID'], 
                                   how = 'left')
    
    # choose the relevant data
    merge_meteo_darksky = merge_meteo_darksky[['DATA_DATETIME', 
                                               'BLOCK_ID', 
                                               'RADIATION', 
                                               'TEMPERATURE', 
                                               'CLOUD_COVER',
                                               'HUMIDITY', 
                                               'PRECIP_PROBABILITY', 
                                               'SURFACE_PRESSURE', 
                                               'WIND_SPEED',
                                               'POWER']]
    
    # return final required data
    return merge_meteo_darksky

"""      combine data for model building       """

def divide_train_test(nodalId, orgId, pssId, StartDate, 
                      FxDate, headers):
    
    # data = merge_meteo_darksky
    
    # retrive combined data
    data = combine_data(nodalId, orgId, pssId, StartDate, 
                        FxDate, headers)
    
    # create a date column in data
    data['DATA_DATE'] = data['DATA_DATETIME'].str.split(" ", n = 1, 
        expand = True)[0]
    
    # choose FxDate data as test and rest as train
    test_data = data[(data['DATA_DATE'] == FxDate)]
    train_data = data[(data['DATA_DATE'] < FxDate)]
    
    # return data for data processing
    return train_data, test_data

##############################################

## Import date class from datetime module
#from datetime import date
## Returns the current local date
#FxDate = date.today()
#
#test_data.to_csv(
#        constants.base_path + "input_data/extracted_data/" + "Sadashivpet_test_data.csv")
#
#train_data = pd.read_csv(
#        constants.base_path + "input_data/extracted_data/" + "Sadashivpet_train_data.csv")
#
#"""      actual radiation       """
## extract actual radiation for n-days
#actual_rad = data_extraction.actual_radiation(nodalId, 
#                                              orgId, 
#                                              pssId, 
#                                              StartDate, 
#                                              FxDate, 
#                                              constants.headers)
#
##############################################
