## Sadashivpet
nodalId = 233 
orgId = 32
pssId = 92
FxDate = "2021-05-20"
StartDate = "2020-05-01"




import numpy as np
import pandas as pd
from datetime import datetime

##Actual data Radiation
df_actual=pd.read_csv("/home/sachin/Documents/sadashivpet/Sadashivpet_Actual_Rad.csv")
data_actual=df_actual[["BLOCK_ID","DATA_DATETIME","RADIATION_ACTUAL"]]
data_actual[["DATE","TIME"]]=data_actual["DATA_DATETIME"].str.split(" ",n = 1, expand = True)
data_actual=data_actual.pivot(index ='BLOCK_ID', columns ='DATE', values ="RADIATION_ACTUAL")
data_actual["BLOCK_ID"]=data_actual.index

##Un-updated data Radiation
df_unupdated=pd.read_csv("/home/sachin/Documents/sadashivpet/Sadashivpet_Meteo_Rad_un-updated.csv")
data_unupdated=df_unupdated[["BLOCK_ID","DATA_DATETIME","RADIATION"]]
data_unupdated[["DATE","TIME"]]=data_unupdated["DATA_DATETIME"].str.split(" ",n = 1, expand = True)
data_unupdated=data_unupdated.pivot(index ='BLOCK_ID', columns ='DATE', values ="RADIATION")
data_unupdated["BLOCK_ID"]=data_unupdated.index



##Updated data Radiation
df_updated=pd.read_csv("/home/sachin/Documents/sadashivpet/Sadashivpet_Meteo_Rad_Updated.csv")
data_updated=df_updated[["BLOCK_ID","DATA_DATETIME","RADIATION"]]
data_updated[["DATE","TIME"]]=data_updated["DATA_DATETIME"].str.split(" ",n = 1, expand = True)
data_updated=data_updated.pivot(index ='BLOCK_ID', columns ='DATE', values ="RADIATION")
data_updated["BLOCK_ID"]=data_updated.index

common_col=data_unupdated.columns.intersection(data_updated.columns)

data_unupdated=data_unupdated[common_col]
data_updated=data_updated[common_col]
# diff_df=data_updated.copy()
diff_df=pd.DataFrame(data_unupdated["BLOCK_ID"])
for col in common_col:
    diff_df[col]=data_updated[col]-data_unupdated[col]
  
diff=diff_df.iloc[:,-100:]
diff.insert(0,"BLOCK_ID",data_unupdated["BLOCK_ID"])
data_unupdated=data_unupdated[diff.columns.intersection(data_unupdated.columns)]

finaldf=pd.DataFrame(data_unupdated["BLOCK_ID"])
fcol=diff.columns.intersection(data_unupdated.columns)
for i in range(len(fcol)-3):
    finaldf[fcol[i+3]]=data_unupdated[fcol[i+3]]+0.7*diff[fcol[i+3-1]]+0.3*diff[fcol[i+3-2]]
    
finaldf=finaldf[finaldf.columns.intersection(data_actual.columns)]
data_updated=data_updated[finaldf.columns.intersection(data_actual.columns)]
data_actual=data_actual[finaldf.columns.intersection(data_actual.columns)]


new_rad=pd.DataFrame(data_unupdated["BLOCK_ID"])
for col in finaldf.columns:
    new_rad[col]=abs(data_actual[col]-finaldf[col])
new_rad["BLOCK_ID"]=data_unupdated["BLOCK_ID"]
new_rad=new_rad[new_rad.iloc[:,1:].values.sum(axis=1) != 0]  


updated_rad=pd.DataFrame(data_unupdated["BLOCK_ID"])
for col in finaldf.columns:
    updated_rad[col]=abs(data_actual[col]-data_updated[col])
updated_rad["BLOCK_ID"]=data_unupdated["BLOCK_ID"]
updated_rad=updated_rad[updated_rad.iloc[:,1:].values.sum(axis=1) != 0]  

new_rad=new_rad[new_rad.BLOCK_ID.isin( range(42,60))]
updated_rad=updated_rad[updated_rad.BLOCK_ID.isin( range(42,60))]
data_actual=data_actual[data_actual.BLOCK_ID.isin( range(42,60))]


d = {'new_rad_mape': new_rad.sum(axis=0)/18, 'updated_rad_mape': updated_rad.sum(axis=0)/18}
mape = pd.DataFrame(data=d)
mape["did_better"]=np.where(mape["new_rad_mape"]<mape["updated_rad_mape"],1,0)
print(mape.did_better.value_counts())

all(finaldf.columns==data_updated.columns)
all(data_actual.columns==data_updated.columns)



finaldf=finaldf[finaldf.values.sum(axis=1) != 0]     






# FxDate="2021-05-01"
# dates= [str(x.date()) for x in list(pd.date_range(end=FxDate,periods=4))]
# unupdated = data_unupdated[data_unupdated.columns.intersection(dates).insert(0,"BLOCK_ID")]
# updated= data_updated[data_unupdated.columns.intersection(dates).insert(0,"BLOCK_ID")]
# actual= data_actual[["BLOCK_ID",FxDate]]
# diff=pd.DataFrame()
# # diff["BLOCK_ID"]=data_actual["BLOCK_ID"]
# for x in unupdated.columns.intersection(updated.columns)[1:]:
#     diff[f"diff_{x}"]=unupdated[x]-updated[x]
 
# train=diff[diff.values.sum(axis=1) != 0]    
# from sklearn.preprocessing import StandardScaler


# scaler = StandardScaler()
# scaled_data = pd.DataFrame(scaler.fit_transform(train),columns=diff.columns)  
# from sklearn.linear_model import LinearRegression
# model = LinearRegression() 
# x=scaled_data.loc[:,scaled_data.columns!=f"diff_{FxDate}"]
# y=scaled_data[[f"diff_{FxDate}"]]
# model.fit(x, y)
# # model.score(x, y)
# scaled_data["diff_estimated"]=model.predict(x)    


# import matplotlib.pyplot as plt

# plt.scatter(scaled_data.diff_estimated, scaled_data[f"diff_{FxDate}"])
# plt.show()

# ###########################################################
# FxDate="2021-05-01"
# dates= [str(x.date()) for x in list(pd.date_range(end=FxDate,periods=20))]
# dfactual=data_actual[data_actual.DATE.isin(dates)]
# dfunupdated=data_unupdated[data_unupdated.DATE.isin(dates)]
# dfupdated=data_updated[data_updated.DATE.isin(dates)]

# #############################
# dfactual=dfactual[['DATA_DATETIME', 'RADIATION_ACTUAL']]
# dfunupdated=dfunupdated[['DATA_DATETIME', 'RADIATION']]
# dfunupdated.columns=['DATA_DATETIME', 'RADIATION_unupdated']
# dfupdated=dfupdated[['DATA_DATETIME', 'RADIATION']]
# dfupdated.columns=['DATA_DATETIME', 'RADIATION_updated']
# AUn=dfactual.merge(dfunupdated, how='inner', on='DATA_DATETIME')
# AUnU=AUn.merge(dfupdated, how='inner', on='DATA_DATETIME')

# AUnU.to_csv("sadashivpetUNU_raddata1.csv",index=False)

    
