import constants
import requests
import pandas as pd
import numpy as np
import json
from datetime import datetime
headers = constants.headers

""" Encoding of input variables to their respective class """

class NpEncoder(json.JSONEncoder):
    # function for assigning class data type
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj) # returning integer
        elif isinstance(obj, np.floating):
            return float(obj) # returning float
        elif isinstance(obj, np.ndarray):
            return obj.tolist() # returning tolist
        else:
            return super(NpEncoder, self).default(obj) # returning default

""" Calculate time differnce in seconds between datetime and entrytime """

def Tdiff(data):
    
    if pd.isnull(data.ENTRY_TIME):
        result = constants.hour_const + 1
    else:
        # entry time formating for different weather source
        one_col = datetime.strptime(data.DATA_DATETIME, '%Y-%m-%d %H:%M:%S')
        two_col = datetime.strptime(str(data.ENTRY_TIME), '%Y-%m-%d %H:%M:%S')
        
        # calculate time differnce values in seconds
        result = ((one_col-two_col).total_seconds())

    # return time differnce values in seconds
    return result

""" Calculate block from datetime column """

def get_block_from_time(now):
    
    a = now.hour    # extract hour from date
    b = now.minute   # extract minute from date
    block = 4*a+1 + b//15    # calculate block
    
    return block

""" generate block and time at 15 mins """

def missing_blocks_processing(data):
    
    # generate n-days with 15 mins granularity
    max_date = data['DATA_DATETIME'].max().split()[0]
    min_date = data['DATA_DATETIME'].min().split()[0]
    datelist = pd.date_range(start = pd.Timestamp(min_date + 'T00'), 
                             end = pd.Timestamp(max_date + 'T23:45'), 
                             freq = constants.freq_granularity)
    df = pd.DataFrame({"DATA_DATETIME": datelist})
    df['BLOCK_ID'] = df['DATA_DATETIME'].apply(lambda x: get_block_from_time(x))
    df['DATA_DATETIME'] = df['DATA_DATETIME'].astype(str)
    
    return df

""" extracted data processing steps """

def extract_processing(data, replace_column):
    
    # generate all the blocks and drop ENTRY_TIME attribute
    df = missing_blocks_processing(data)
    
    # prepare a data frame with all values
    merged_data = pd.merge(df, data, 
                           on = ['DATA_DATETIME', 'BLOCK_ID'], 
                           how = 'left')
    
    # replace all the values to zero above 75th and 22nd block
    merged_data.loc[merged_data.BLOCK_ID >= 75, replace_column] = 0
    merged_data.loc[merged_data.BLOCK_ID <= 22, replace_column] = 0
    
    # to interpolate the missing values
    df_final = merged_data.interpolate(method = 'piecewise_polynomial', 
                                       limit_direction = 'backward', 
                                       order = 5)
    
    return df_final

""" function to subset data for a day """

def subset_day_data(data, chosen_date):
    
    # subseting data for day and bind it final data frame
    tt = data['DATA_DATETIME'].str.split(" ", n = 1, expand = True)
    data = data[tt[0] == chosen_date]
    data = data.reset_index(drop = True)
    
    return data

""" METEOTEST data extraction """

def meteotest_weather(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    meteo_api_farm = "rerfwfxmeteotestcloudmv"
    meteo_api_pss = "rerfwfxmeteotestcloudmvpss"
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    final_meteo_rad = pd.DataFrame(columns = ['BLOCK_ID',
                                              'DATA_DATETIME',
                                              'RADIATION', 
                                              'TEMPERATURE',
                                              'ENTRY_TIME'])
    
    # Loop for extraction meteo data for required dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        # i = "2020-05-01"
        
        # extract data @ pss level 
        if nodalId == -1:
          meteo_wsapi=f"{constants.api_base}/{meteo_api_pss}/"
          payload = json.dumps({
            "orgId": orgId,
            "pssId": pssId,
            "startDate": i
          }, cls=NpEncoder)
        else:   # extract data @ farm level
          meteo_wsapi = f"{constants.api_base}/{meteo_api_farm}/"
          payload = json.dumps({
            "farmId": nodalId,
            "startDate": i,
          }, cls=NpEncoder)  
        
        # get the data through POST method
        response = requests.request("POST", meteo_wsapi, 
                                    headers = headers, 
                                    data = payload)
        x = response.json()     # convert data into JSON
        
        # error handling of non-availability of data
        if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
            continue
        else:
            meteo_rad = pd.DataFrame(x['data'])    # extract required data
            
            # subset required columns
            meteo_rad = meteo_rad[['BLOCK_ID',
                                   'DATA_DATETIME',
                                   'GLOBAL_RADIATION_HORIZONTAL_W_M',
                                   'AIR_TEMP_CELSIUS',
                                   'ENTRY_TIME']]
            
            # rename RADIATION & TEMPERATURE columns
            meteo_rad = meteo_rad.rename(columns={
                    'GLOBAL_RADIATION_HORIZONTAL_W_M': 'RADIATION'})
            meteo_rad = meteo_rad.rename(columns={
                    'AIR_TEMP_CELSIUS': 'TEMPERATURE'})
            
            # rbind data for all required dates
            final_meteo_rad = final_meteo_rad.append(pd.DataFrame(data = 
                                                                  meteo_rad),
                                                     ignore_index=True)
    
    # choose the un-updated data for radiation
    final_meteo_rad["time_diff"] = final_meteo_rad.apply(Tdiff, axis = 1)
    data = final_meteo_rad[final_meteo_rad["time_diff"] >= constants.hour_const]
    data = data.sort_values("time_diff").groupby("DATA_DATETIME",
                           as_index = False).first()
    data.drop(["time_diff"], axis = 'columns', inplace = True)
    
    # generate all the blocks and drop ENTRY_TIME attribute
    df_final = extract_processing(data, 'RADIATION')
    df_final.drop(['ENTRY_TIME'], axis = 'columns', inplace = True)
    
    # change the format
    df_final = df_final.astype({'TEMPERATURE': 'float64', 
                                'RADIATION': 'float64'})
    
    # remove all negatives from data frame
    df_final[['TEMPERATURE','RADIATION']] = df_final[['TEMPERATURE','RADIATION'
            ]].mask(df_final[['TEMPERATURE','RADIATION']] < 0, 0)
    
    return df_final

""" DARKSKY data extraction """

def darksky_weather(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate, 
                              freq = 'M').strftime('%Y-%m-%d')
    seq_dates = seq_dates.insert(0, StartDate)
    seq_dates = list(seq_dates)
    seq_dates.append(FxDate)
    
    # Create the pandas DataFrame
    final_darksky_data = pd.DataFrame(columns = ['BLOCK_ID',
                                                 'DATA_DATETIME',
                                                 'WIND_SPEED',
                                                 'PRECIP_INTENSITY',
                                                 'PRECIP_PROBABILITY',
                                                 'DEW_PT_TEMP',
                                                 'HUMIDITY',
                                                 'SURFACE_PRESSURE',
                                                 'CLOUD_COVER',
                                                 'ENTRY_TIME'])
    
    # define api
    darksky_api_farm = "rerfwfxdarkskyfx"
    darksky_api_pss = "rerfwfxdarkskyfxpss"
    
    for i in range(len(seq_dates)-1):
            
        # extract data @ pss level
        if nodalId == -1:
            darksky_wsapi = f"{constants.api_base}/{darksky_api_pss}/"
            payload = json.dumps({
                "orgId": orgId,
                "pssId": pssId,
                "startDate": seq_dates[i],
                "endDate": seq_dates[i+1]
                }, cls=NpEncoder)
        else:   # extract data @ farm level
            darksky_wsapi=f"{constants.api_base}/{darksky_api_farm}/"
            payload = json.dumps({
                "farmId": nodalId,
                "startDate": seq_dates[i],
                "endDate": seq_dates[i+1]
                }, cls=NpEncoder)
        
        # subset required columns
        response = requests.request("POST", 
                                    darksky_wsapi, 
                                    headers = headers, 
                                    data = payload)
        x = response.json()
        # data = [json.loads(line) for line in open('data.json', 'r')]
        
        if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
            # Create the pandas DataFrame
            data = pd.DataFrame(columns = ['BLOCK_ID',
                                           'DATA_DATETIME',
                                           'WIND_SPEED',
                                           'PRECIP_INTENSITY',
                                           'PRECIP_PROBABILITY',
                                           'DEW_PT_TEMP',
                                           'HUMIDITY',
                                           'SURFACE_PRESSURE',
                                           'CLOUD_COVER',
                                           'ENTRY_TIME'])
            pass
        else:
            darksky_data = pd.DataFrame(x['data'])    # extract required data
            
            # subset required columns
            df = darksky_data[['BLOCK_ID',
                               'DATA_DATETIME',
                               'WIND_SPEED',
                               'PRECIP_INTENSITY',
                               'PRECIP_PROBABILITY',
                               'DEW_PT_TEMP',
                               'HUMIDITY',
                               'SURFACE_PRESSURE',
                               'CLOUD_COVER',
                               'ENTRY_TIME']]
            
            # Get time differnce for subseting un-updated data
            df['ENTRY_TIME'] = df['ENTRY_TIME'].apply(lambda x: 
                datetime.strptime(x,'%Y-%m-%d %H:%M:%S.%f'))
            df['DATA_DATETIME'] = df['DATA_DATETIME'].apply(lambda x:
                datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))
            df["time_diff"]=(df.DATA_DATETIME - df.ENTRY_TIME
              ).astype('timedelta64[s]')
            
            # Choose un-updated darksky data
            data = df[df["time_diff"] >= constants.hour_const]
            data = data.sort_values("time_diff").groupby("DATA_DATETIME",
                                   as_index=False).first()
            data.drop(["time_diff"], axis = 'columns', inplace = True)
            data['DATA_DATETIME'] = data['DATA_DATETIME'].astype(str)
            data['ENTRY_TIME'] = data['ENTRY_TIME'].astype(str)
            
        # rbind data for all required dates
        final_darksky_data = final_darksky_data.append(
                pd.DataFrame(data = data), ignore_index = True)
    
    return final_darksky_data

""" ACTUAL RADIATION extraction """

def actual_radiation(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    actrad_api_farm = "rerfswact"   
    actrad_api = f"{constants.api_base}/{actrad_api_farm}/"
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    act_rad = pd.DataFrame(columns = ['BLOCK_ID',
                                      'DATA_DATETIME',
                                      'RADIATION_ACTUAL'])
    
    # Loop for extraction meteo data for reuired dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        
        # define input parameters for API
        payload = json.dumps({
            "farmId": nodalId,
            "startDate": i
        }, cls=NpEncoder)
        
        # query the data from API
        response = requests.request("POST", 
                                    actrad_api, 
                                    headers = headers, 
                                    data = payload)
        x = response.json()
        
        if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
            continue
        else:
            df = pd.DataFrame(x['data'])
        
            # subset required columns
            df = df[['BLOCK_ID', 'DATATIME', 'RAD_ACT']]
            
            # rename the date time column
            df = df.rename(columns={'RAD_ACT': 'RADIATION_ACTUAL',
                                    'DATATIME': 'DATA_DATETIME'})
            
            # subseting data for day and bind it final data frame
            df = subset_day_data(df, i)
            
            # generate all the blocks and drop ENTRY_TIME attribute
            df_final = extract_processing(df, 'RADIATION_ACTUAL')
            
            # rbind data for all required dates
            act_rad = act_rad.append(pd.DataFrame(data = df_final), 
                                     ignore_index = True)
        
    return act_rad

""" ACTUAL POWER extraction """

def actual_power(nodalId, orgId, pssId, StartDate, FxDate, headers):
        
    # define api
    actpower_api_farm = "rerfsdiurnal"
    actpower_api_pss = "rerfsdiurnalpss"
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    act_power = pd.DataFrame(columns = ['BLOCK_ID',
                                        'DATA_DATETIME',
                                        'POWER'])
    
    # Loop for extraction meteo data for reuired dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        
        # extract data @ pss level
        if nodalId == -1:
            actpower_api = f"{constants.api_base}/{actpower_api_pss}/"
            payload = json.dumps({
                "orgId": orgId,
                "pssId": pssId,
                "startDate": i
                }, cls=NpEncoder)
        else:   # extract data @ farm level
            actpower_api = f"{constants.api_base}/{actpower_api_farm}/"
            payload = json.dumps({
                "farmId": nodalId,
                "startDate": i
                }, cls=NpEncoder)
        
        # query the data from API    
        response = requests.request("POST", 
                                    actpower_api, 
                                    headers = headers, 
                                    data = payload)
        x = response.json()
        
        if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
            continue
        else:
            df = pd.DataFrame(x['data'])
        
            # rename the date time column
            df = df.rename(columns={'DATATIME': 'DATA_DATETIME'})
            
            # subset required columns
            df = df[['BLOCK_ID', 'DATA_DATETIME', 'POWER']]
            
            # subseting data for day and bind it final data frame
            df = subset_day_data(df, i)
                    
            # generate all the blocks and drop ENTRY_TIME attribute
            df_final = extract_processing(df, 'POWER')
                    
            # rbind data for all required dates
            act_power = act_power.append(pd.DataFrame(data = df_final), 
                                         ignore_index = True)
        
    return act_power    
